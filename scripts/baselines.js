/**
 * Uses PhantomJs to take the baseline screenshots of the WTA site.
 * The images taken by this test suite are saved to the screenshots/baseline folder and are the ones
 * the visual-tests compare against.
 */

var path = require('path');

module.exports = {
    before: browser => {
        let screenWidth = browser.options.desiredCapabilities.screenWidth;
        let screenHeight = browser.options.desiredCapabilities.screenHeight;
        console.log(`Resizing window to ${screenWidth} x ${screenHeight}`);
        browser.resizeWindow(screenWidth, screenHeight);
    },

    /**
     * Takes a snapshot of the entire home page and saves it to the screenshots/baseline/home.png file.
     */
    'Taking baseline of Home': browser => {
        console.log('Taking baseline images of home from site: ' + browser.launchUrl);
        browser.url(browser.launchUrl)
            .waitForDocumentLoaded(10000, 'Page loaded.')
            .saveScreenshot(path.join(__dirname, '..', 'screenshots', 'baseline', 'home.png'));
    },

    after: browser => {
        browser.end();
    }
};
