/**
 * A visual regression test of the site.
 */

var fs = require('fs');
var path = require('path');
var resemble = require('resemble').resemble;

let baselinePath = path.join(__dirname, '..', 'screenshots', 'baseline');
let testsPath = path.join(__dirname, '..', 'screenshots', 'tests');
let diffPath = path.join(__dirname, '..', 'screenshots', 'diffs');

let comparisonCallback = function (imgName, comparisonResult) {
    let diffBuffer = new Buffer(comparisonResult.getImageDataUrl()
        .replace(/data:image\/png;base64,/, ''), 'base64');
    fs.writeFileSync(path.join(diffPath, imgName), diffBuffer);
    console.log('Dimension difference: ' + JSON.stringify(comparisonResult.dimensionDifference));
    console.log('% difference: ' + comparisonResult.misMatchPercentage);
};

module.exports = {

    /**
     * Resize browser window to get consistent visual tests across browsers and pages.
     */
    before: browser => {
        let screenWidth = browser.options.desiredCapabilities.screenWidth;
        let screenHeight = browser.options.desiredCapabilities.screenHeight;
        console.log(`Resizing window to ${screenWidth} x ${screenHeight}...`);
        browser.resizeWindow(screenWidth, screenHeight);
        console.log('Taking screenshots from: ' + browser.launchUrl);
    },

    /**
     * Takes a full snapshot of the site configured in nightwatch.conf.js (look at the README of this project to learn
     * how to change the URL). The image taken in this test case is compared against home.png of the baselines
     * and a difference percentage is printed to the console. This difference can be compared against a threshold
     * in order to fail the test.
     */
    'Taking screenshot of Home': browser => {
        browser.url(browser.launchUrl)
            .waitForElementVisible('#superfish-1', 5000, 'Waiting for menu to be visible.')
            .saveScreenshot(path.join(testsPath, 'home.png'), function (saveResult) {
                browser.assert.equal(saveResult.state, 'success', 'Save home screenshot success.');
                let baselineFile = path.join(baselinePath, 'home.png');
                let testFile = path.join(testsPath, 'home.png');
                console.log('Comparing ' + baselineFile + ' with ' + testFile);

                resemble.outputSettings({errorType: 'flat', transparency: 1})(baselineFile).compareTo(testFile)
                    .onComplete(comparisonCallback.bind(this, "home.png"));
            });
    },

    after: browser => {
        browser.end();
    }
};
