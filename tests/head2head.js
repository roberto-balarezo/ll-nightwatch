/**
 * Example test suite to navigate to the Head to Head page and compare players.
 */

let urljoin = require('url-join');

const CSS = 'css selector';

/**
 * Wrapper callback that validates the result.status is zero to continue executing the code. if it is not, print
 * the result to standard error, so we can know the reason.
 * Also, as a convenience, the argument passed to the function is directly the value returned by the command
 * (result.value).
 * @param errorCallback A callback to call in case the result status is nonzero, and before the exception is thrown.
 */
function validateResult(originalCallback, errorCallback) {
    return function(result) {
        if (result.status !== 0) {
            console.error(result);
            if (errorCallback !== undefined && typeof errorCallback === 'function') {
                errorCallback();
            }
            throw new Error('Error executing command!');
        }
        originalCallback.call(this, result.value);
    };
}

module.exports = {
    /**
     * Initialize browser.
     */
    before: browser => {
        let screenWidth = browser.options.desiredCapabilities.screenWidth;
        let screenHeight = browser.options.desiredCapabilities.screenHeight;
        console.log(`Resizing window to ${screenWidth} x ${screenHeight}...`);
        browser.resizeWindow(screenWidth, screenHeight);
        browser.url(browser.launchUrl);
        browser.waitForDocumentLoaded(10000, 'Page loaded.');
    },

    'Navigate to Head to Head page using the menu': browser => {
        const playersMenuSelector = '#superfish-1 #menu-1030-1 > a';
        const submenuSelector = '#superfish-1 #rm-no-id-2';
        const head2headSelector = `${submenuSelector} li#menu-1818-1 > a`;
        browser.moveToElement(playersMenuSelector, 0, 0);
        browser.waitForElementVisible(submenuSelector, 1000, 'Submenu visible.');
        browser.click(head2headSelector);
        browser.waitForDocumentLoaded(10000, 'Destination page loaded.');
        // Check we are really on the head2head page
        browser.url(validateResult(function(result) {
            this.assert.urlContains('headtohead');
        }));
    },

    'Check some elements of the page are present': browser => {
        let playerContainer = '.h2h-tb-playera';
        let playerWinsSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-stats .h2h-tbps-wins`;
        let playerPercentageSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-stats .h2h-tbps-pct`;
        let playerImageSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-image picture img`;
        let playerGoToBtnSelector = `#h2h-tb-container ${playerContainer} .button-gtprofile`;

        browser.assert.visible(playerWinsSelector, 'First player visible.');
        browser.getText(playerWinsSelector, validateResult(function(textResult) {
            this.assert.strictEqual(/^\d+$/.test(textResult.trim()), true, 'Number of wins is indeed numeric.');
        }));
        browser.assert.visible(playerPercentageSelector, 'Percentage wins of first player is visible.');
        browser.getText(playerPercentageSelector, validateResult(function(textResult) {
            this.assert.strictEqual(/^\d+ %$/.test(textResult.trim()), true, 'Win percentage is in expected format.');
        }));
        browser.assert.visible(playerImageSelector, 'First player image is visible.');
        browser.assert.visible(playerGoToBtnSelector, 'First player profile link is visible.');

        playerContainer = '.h2h-tb-playerb';
        playerWinsSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-stats .h2h-tbps-wins`;
        playerPercentageSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-stats .h2h-tbps-pct`;
        playerImageSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-image picture img`;
        playerGoToBtnSelector = `#h2h-tb-container ${playerContainer} .button-gtprofile`;

        browser.assert.visible(playerWinsSelector, 'Second player visible.');
        browser.getText(playerWinsSelector, validateResult(function(textResult) {
            this.assert.strictEqual(/^\d+$/.test(textResult.trim()), true, 'Number of wins of player 2 is indeed numeric.');
        }));
        browser.assert.visible(playerPercentageSelector, 'Percentage wins of second player is visible.');
        browser.getText(playerPercentageSelector, validateResult(function(textResult) {
            this.assert.strictEqual(/^\d+ %$/.test(textResult.trim()), true, 'Win percentage of player 2 is in expected format.');
        }));
        browser.assert.visible(playerImageSelector, 'Second player image is visible.');
        browser.assert.visible(playerGoToBtnSelector, 'Second player profile link is visible.');
    },

    'Compare other players': browser => {
        const firstPlayerBox = '.search-player .headtohead-form-playera input[name="playera"]';
        const secondPlayerBox = '.search-player .headtohead-form-playerb input[name="playerb"]';

        // First player
        browser.click(firstPlayerBox).pause(250);
        // Check the search box is cleaned after click
        browser.getValue(firstPlayerBox, validateResult(function(valueResult) {
            this.assert.equal(valueResult, '', 'Player name field cleaned when clicked.');
        }));
        browser.setValue(firstPlayerBox, ['serena williams']);
        // Pause until autocomplete request is triggered
        browser.pause(500);

        // Wait for autocomplete request...
        browser.waitForJqueryAjaxRequest(5000);

        // Find the autocomplete popup and select the first result.
        let popupSelector = null;
        let firstResultSelector = null;
        let autocompleteListsCount = 0;
        browser.elements(CSS, 'ul.ui-widget.ui-autocomplete', validateResult(function(result) {
            this.assert.strictEqual(result.length >= 2, true, 'Autocomplete lists found.');
            autocompleteListsCount = result.length;
            popupSelector = '#ui-id-1';
            firstResultSelector = `${popupSelector} li:first-child div`;
        }));
        // We need to use perform so the code executes after we have populated the firstResultSelector variable
        // with the previous step result.
        browser.perform(function(client, done) {
            client.assert.visible(popupSelector, 'Popup is visible.');
            client.getLocationInView(popupSelector);
            client.moveToElement(firstResultSelector, 0, 0);
            client.click(firstResultSelector);
            done();
        });

        // Now the same steps for the second player
        browser.click(secondPlayerBox).pause(250);
        browser.getValue(secondPlayerBox, validateResult(function(valueResult) {
            this.assert.equal(valueResult, '', 'Second player name field cleaned when clicked.');
        }));
        browser.setValue(secondPlayerBox, ['maria sharapova']).pause(500);
        browser.waitForJqueryAjaxRequest(5000);
        browser.perform(function(client, done) {
            popupSelector = '#ui-id-2';
            firstResultSelector = `${popupSelector} li:first-child div`;
            client.assert.visible(popupSelector, 'Popup is visible.');
            client.getLocationInView(popupSelector);
            client.moveToElement(firstResultSelector, 0, 0);
            client.click(firstResultSelector);
            done();
        });

        // Finally, click the compare button.
        browser.click('.headtohead-compare');
        browser.waitForJqueryAjaxRequest(10000);

        // Keep the browser open for a while to see the results.
        browser.pause(10000);
    },

    /**
     * End of the test. Destroy the browser session and close the browser.
     */
    after: browser => {
        console.log('End tests for where to watch page. Closing browser...');
        browser.end();
    }
};
